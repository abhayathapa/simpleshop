Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      sessions: 'sessions',
      registrations: 'registrations'
    }

  resources :regions, only: %i[create update destroy index]
  resources :products, only: %i[create update destroy index]
  resources :orders, only: %i[create update destroy index]
end
