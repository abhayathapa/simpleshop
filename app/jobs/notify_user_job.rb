class NotifyUserJob < ApplicationJob
  queue_as :default

  def perform(order_id)
    return unless order_id

    order = Order.find(order_id)
    order.update(
      status: [0,1,2,3].sample,
      payment_complete: [true,false].sample
    )
    send_notification(order_id)
  end

  private

  def send_notification(order_id)
    UserMailer.new_order(order_id).deliver
  end
end
