class RegionsController < ApplicationController
  before_action :set_region, only: [:update, :destroy]
  before_action :is_admin?, only: [:create, :update, :destroy]

  # GET /regions
  def index
    respond_with Region.all
  end

  # POST /regions
  def create
    region = Region.new(region_params)
    if region.save
      render json: region, status: :created
    else
      render json: region.errors.messages, status: :unprocessable_entity
    end
  end

  # PUT /regions/:id
  def update
    if @region.update(region_params)
      head :no_content
    else
      render json: @region.errors.messages, status: :unprocessable_entity
    end
  end

  # DELETE /regions/:id
  def destroy
    @region.destroy
    head :no_content
  end

  private

  def region_params
    params.require(:region).permit(:title, :code, :currency, :tax)
  end

  def set_region
    @region = Region.find(params[:id])
  end
end
