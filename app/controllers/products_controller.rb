class ProductsController < ApplicationController
  before_action :set_product, only: [:update, :destroy]
  before_action :is_admin?, only: [:create, :update, :destroy]

  # GET /products
  def index
    respond_with Product.all
  end

  # POST /products
  def create
    product = Product.new(product_params)
    if product.save
      render json: product, status: :created
    else
      render json: product.errors.messages, status: :unprocessable_entity
    end
  end

  # PUT /products/:id
  def update
    if @product.update(product_params)
      head :no_content
    else
      render json: @product.errors.messages, status: :unprocessable_entity
    end
  end

  # DELETE /products/:id
  def destroy
    @product.destroy
    head :no_content
  end

  private

  def product_params
    params.require(:product).permit(:title, :category, :description, :image, :price, :stock, :region_id)
  end

  def set_product
    @product = Product.find(params[:id])
  end
end
