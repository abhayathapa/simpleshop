class OrdersController < ApplicationController

  # GET /orders
  def index
    respond_with Order.all
  end

  # POST /orders
  def create
    order = Order.new(order_params)
    if order.save
      render json: order, status: :created
    else
      render json: order.errors.messages, status: :unprocessable_entity
    end
  end

  # PUT /orders/:id
  def update
    order = Order.find(params[:id])
    if order.update(order_params)
      head :no_content
    else
      render json: order.errors.messages, status: :unprocessable_entity
    end
  end

  private

  def order_params
    params.require(:order).permit(:name, :address, :payment_complete, :phone, :status, :user_id,
      line_items_attributes: [:product_id, :quantity])
  end
end
