class ApplicationController < ActionController::API
  before_action :authenticate_user!

  # @return error if current user is not logged in as admin
  def is_admin?
    unless user_signed_in? && current_user.roles.include?('admin')
      render json: {error: 'You need to be logged in as admin to continue' }.to_json, status: :unauthorized
    end
  end
end
