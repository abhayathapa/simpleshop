class Order < ApplicationRecord
  belongs_to :user, optional: true
  # index_errors to add errors on nested models
  has_many :line_items, index_errors: true, dependent: :destroy
  has_many :products, through: :line_items

  validates_presence_of :name, :phone
  after_create :notify_user
  accepts_nested_attributes_for :line_items

  ## Status of orders
  STATUS = {
    0 => 'Initiated',
    1 => 'Acknowledged',
    2 => 'In Delivery',
    3 => 'Completed'
  }.freeze

  # @return [Float]
  # Total amount calculated from line items
  def total
    line_items.to_a.sum { |item| item.total_price }
  end

  private

  ## Create background job to notify user and update status
  def notify_user
    NotifyUserJob.set(wait: 1.minutes).perform_later(id)
  end
end
