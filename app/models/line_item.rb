class LineItem < ApplicationRecord
  belongs_to :product
  belongs_to :order
  validate :ensure_stock?

  # @return [Float]
  # Total amount of the line item
  def total_price
    product.price * quantity
  end

  private

  # Ensure line item is not created if stock quantity is less than order quantity
  def ensure_stock?
    errors.add(:base, 'Not enought products in the stock') if quantity > product.stock
  end
end
