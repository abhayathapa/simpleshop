class Region < ApplicationRecord
  validates_presence_of :title, :code, :currency
end
