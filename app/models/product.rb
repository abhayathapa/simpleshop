class Product < ApplicationRecord
  belongs_to :region, optional: true
  has_many :line_items, dependent: :destroy

  validates_presence_of :title, :category, :price

  ## create unique sku from title and random number
  before_create do
    self.sku = title[0,4] + SecureRandom.hex(5) if sku.blank?
  end
end
