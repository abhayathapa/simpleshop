class UserMailer < ApplicationMailer
  default from: 'thapa.abhay@gmail.com'

  def new_order(order_id)
    @order = Order.find(order_id)
    mail(to: @order.user.email, subject: 'Your order has been placed')
  end
end
