class ApplicationMailer < ActionMailer::Base
  default from: 'thapa.abhay@gmail.com'
  layout 'mailer'
end
