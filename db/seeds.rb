user_1 = User.create(email: 'john@a.com', password: 'password', password_confirmation: 'password')
user_2 = User.create(email: 'tom@a.com', password: 'password', password_confirmation: 'password')
## Create Admin by adding role from console
admin = User.create(email: 'admin@a.com', password: 'password', password_confirmation: 'password')
admin.roles << 'admin'
admin.save

## create regions
5.times do
  Region.create(
    title: Faker::Address.country,
    code: Faker::Address.country_code ,
    currency: Faker::Currency.symbol,
    tax: [0,5,10,15].sample
  )
end

## create products
10.times do
  Product.create(
    title: Faker::Commerce.product_name,
    category: ['Tshirt','Pant','Suit','Shoe','Socks', 'Dress'].sample,
    description: Faker::Lorem.sentence(1),
    image: Faker::Avatar.image,
    price: Faker::Commerce.price,
    stock: Faker::Number.within(range: 0..20),
    region: Region.all.sample
  )
end

## create orders
5.times do
  Order.create(
    name: Faker::Name.name,
    address: Faker::Address.street_address,
    status: [0,1,2,3].sample,
    payment_complete: [true, false].sample,
    phone: Faker::PhoneNumber.cell_phone,
    user: User.all.sample
  )
end

## create line items
10.times do
  LineItem.create(
    product: Product.all.sample,
    order: Order.all.sample,
    quantity: Faker::Number.within(range: 1..10)
  )
end
