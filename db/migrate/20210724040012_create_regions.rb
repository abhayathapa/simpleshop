class CreateRegions < ActiveRecord::Migration[6.0]
  def change
    create_table :regions do |t|
      t.string :title,  null: false
      t.string :code, null: false
      t.string :currency, index: true, null: false
      t.integer :tax

      t.timestamps
    end
  end
end
