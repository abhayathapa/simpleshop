class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title,  null: false
      t.string :category, null: false, index: true
      t.string :description
      t.string :image
      t.string :price, null: false, index: true
      t.string :sku, null: false
      t.integer :stock, null: false, default: 1
      t.references :region, foreign_key: true

      t.timestamps
    end
  end
end
