class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :name, null: false
      t.string :address
      t.integer :status, default: 0
      t.boolean :payment_complete, default: false
      t.string :phone, null: false

      t.references :user
      t.timestamps
    end
  end
end
