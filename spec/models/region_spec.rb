require 'rails_helper'

RSpec.describe Region, type: :model do
  subject { create(:region) }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without title" do
      subject.title = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without code" do
      subject.code = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without currency" do
      subject.currency = nil
      expect(subject).to_not be_valid
    end
  end
end
