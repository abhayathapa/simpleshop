require 'rails_helper'

RSpec.describe Product, type: :model do
  subject { create(:product) }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without title" do
      subject.title = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without category" do
      subject.category = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without price" do
      subject.price = nil
      expect(subject).to_not be_valid
    end

    it "updates sku of the product automatically" do
      product = build(:product)
      expect{ product.save }.to change{ product.sku }
    end
  end

  describe "Associations" do
    it { expect(described_class.reflect_on_association(:region).macro).to eq(:belongs_to) }
  end
end
