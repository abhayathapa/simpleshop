require 'rails_helper'

RSpec.describe Order, type: :model do
  subject { create(:order) }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without phone" do
      subject.phone = nil
      expect(subject).to_not be_valid
    end
  end

  describe "Associations" do
    it { expect(described_class.reflect_on_association(:user).macro).to eq(:belongs_to) }
  end

  describe '#total' do
    it "calculates total from line_items" do
      product = create(:product)
      line = subject.line_items.create!(product: product, quantity: 2)
      expect(subject.total).to eq(product.price * 2)
    end
  end
end
