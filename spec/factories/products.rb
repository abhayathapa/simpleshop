FactoryBot.define do
  factory :product do
    title { Faker::Commerce.product_name }
    category { ['Tshirt','Pant','Suit','Shoe','Socks', 'Dress'].sample }
    description { Faker::Lorem.sentence(1) }
    image { Faker::Avatar.image }
    price { Faker::Commerce.price }
    stock { Faker::Number.within(range: 0..20) }
    association :region
  end
end
