FactoryBot.define do
  factory :order do
    name { Faker::Name.name }
    address { Faker::Address.street_address }
    status { [0,1,2,3].sample }
    payment_complete { [true, false].sample }
    phone { Faker::PhoneNumber.cell_phone }
    association :user
  end
end
