FactoryBot.define do
  factory :region do
    title { Faker::Address.country }
    code { Faker::Address.country_code }
    currency { Faker::Currency.symbol }
    tax { [0,5,10,15].sample }
  end
end
