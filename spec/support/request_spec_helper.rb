module RequestSpecHelper

  include Warden::Test::Helpers

  def admin_sign_in
    user = User.create(email: 'a@a.com', password: Faker::Internet.password, roles: ['admin', 'customer'])
    user.confirm
    sign_in(user)
  end

  def user_sign_in
    user = User.create(email: 'a@a.com', password: Faker::Internet.password)
    user.confirm
    sign_in(user)
  end

  def sign_out(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    logout(scope)
  end

  private

  def sign_in(resource_or_scope, resource = nil)
    resource ||= resource_or_scope
    scope = Devise::Mapping.find_scope!(resource_or_scope)

    login_as(resource, scope: scope)
  end
end
