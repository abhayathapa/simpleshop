require 'rails_helper'

RSpec.describe UserMailer, :type => :mailer do
  describe "#new_order" do
    let(:order) { create(:order) }
    let(:mail) { UserMailer.new_order(order.id) }

    it "renders headers" do
      expect(mail.subject).to eq("Your order has been placed")
      expect(mail.to).to eq([order.user.email])
      expect(mail.from).to eq(["thapa.abhay@gmail.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Name: #{order.name}")
    end
  end
end
