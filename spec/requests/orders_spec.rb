require 'rails_helper'

RSpec.describe "Orders", type: :request do
  before(:each) do
    user_sign_in
  end

  describe 'POST /orders' do
    let(:user) { create(:user) }
    let(:product_1) { create(:product, stock: 5) }
    let(:product_2) { create(:product, stock: 7) }
    let(:order) { create(:order) }

    context 'when request attributes are valid' do
      let(:valid_attributes) { {
        "order": {
            "name": Faker::Name.name,
            "address": Faker::Address.street_address,
            "status": [0,1,2,3].sample,
            "payment_complete":[true, false].sample,
            "phone": Faker::PhoneNumber.cell_phone,
            "line_items_attributes": [ {product_id: product_1.id, quantity: 1},
              {product_id: product_2.id, quantity: 2} ],
            "user_id": user.id
        },
      }}
      before { post "/orders", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'creates a line items' do
        expect(JSON.parse(response.body)['name']).to eq(Order.first.name)
        expect(Order.first.line_items.count).to eq 2
      end
    end

    context 'when an invalid request' do
      let(:invalid_attributes) { {
        "order": {
          "address": Faker::Address.street_address,
          "status": [0,1,2,3].sample,
          "payment_complete":[true, false].sample,
          "phone": Faker::PhoneNumber.cell_phone,
          "line_items": [ {product_id: product_1.id, quantity: 1},
            {product_id: product_2.id, quantity: 2} ]
      },
      }}
      before { post "/orders", params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(JSON.parse(response.body)).to eq({"name"=>["can't be blank"]})
      end
    end

    context 'when order quantity exceed stock' do
      let(:invalid_attributes) { {
        "order": {
          "name": Faker::Name.name,
          "address": Faker::Address.street_address,
          "status": [0,1,2,3].sample,
          "payment_complete":[true, false].sample,
          "phone": Faker::PhoneNumber.cell_phone,
          "line_items_attributes": [ {product_id: product_1.id, quantity: 10},
            {product_id: product_2.id, quantity: 2} ],
          "user_id": user.id
      },
      }}
      before { post "/orders", params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(JSON.parse(response.body).values.flatten).to eq(["Not enought products in the stock"])
      end
    end
  end
end
