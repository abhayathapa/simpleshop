require 'rails_helper'

RSpec.describe "Regions", type: :request do
  before(:each) do
    admin_sign_in
  end

  describe 'POST /regions' do
    context 'when request attributes are valid' do
      let(:valid_attributes) { {
        "region": {
            "title":  Faker::Address.country,
            "code": Faker::Address.country_code,
            "currency": Faker::Currency.symbol,
            "tax": 10
        }
      }}
      before { post "/regions", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      let(:invalid_attributes) { {
        "region": {
            "code": "TH",
            "currency": "$",
            "tax": 10
        }
      }}
      before { post "/regions", params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(JSON.parse(response.body)).to eq({"title"=>["can't be blank"]})
      end
    end
  end

  describe 'PUT /regions/:id' do
    let(:region) { create(:region) }
    let(:valid_attributes) { {
      "region": {
          "title":  Faker::Address.country,
          "code": Faker::Address.country_code,
          "currency": Faker::Currency.symbol,
          "tax": 10
      }
    }}

    context 'when the record exists' do
      before { put "/regions/#{region.id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /regions/:id' do
    let(:region) { create(:region) }
    before { delete "/regions/#{region.id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    it 'changes product count by 1' do
      expect(Region.count).to be 0
    end
  end
end
