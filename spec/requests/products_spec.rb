require 'rails_helper'

RSpec.describe "Products", type: :request do
  before(:each) do
    admin_sign_in
  end

  describe 'POST /products' do
    let(:region) { create(:region) }

    context 'when request attributes are valid' do
      let(:valid_attributes) { {
        "product": {
            "title": Faker::Commerce.product_name,
            "category": ['Tshirt','Pant','Suit','Shoe','Socks', 'Dress'].sample,
            "description": Faker::Lorem.sentence(1) ,
            "image": Faker::Avatar.image,
            "price": Faker::Commerce.price,
            "region_id": region.id
        }
      }}
      before { post "/products", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'creates a product' do
        expect(JSON.parse(response.body)['title']).to eq(Product.first.title)
      end
    end

    context 'when an invalid request' do
      let(:invalid_attributes) { {
        "product": {
          "title": Faker::Commerce.product_name,
          "category": ['Tshirt','Pant','Suit','Shoe','Socks', 'Dress'].sample,
          "description": Faker::Lorem.sentence(1) ,
          "image": Faker::Avatar.image,
          "region": region.id
      }
      }}
      before { post "/products", params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(JSON.parse(response.body)).to eq({"price"=>["can't be blank"]})
      end
    end
  end

  describe 'PUT /products/:id' do
    let(:region) { create(:region) }
    let(:product) { create(:product) }
    let(:valid_attributes) { {
      "product": {
          "title": Faker::Commerce.product_name,
          "category": ['Tshirt','Pant','Suit','Shoe','Socks', 'Dress'].sample,
          "description": Faker::Lorem.sentence(1) ,
          "image": Faker::Avatar.image,
          "price": Faker::Commerce.price,
          "region": region.id
      }
    }}

    context 'when the record exists' do
      before { put "/products/#{product.id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /products/:id' do
    let(:product) { create(:product) }
    before { delete "/products/#{product.id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    it 'changes product count by 1' do
      expect(Product.count).to be 0
    end
  end
end
